package homeworks.collections;

import java.util.ArrayList;
import java.util.List;

public class Generics {

    public static void main(String[] args) {
        //////////////java5//////////
        List animals = new ArrayList();
        Animal ourAnimal = new Animal();
        animals.add("cat");
        animals.add("dog");
        animals.add("frog");
        animals.add(ourAnimal);

         String animal = (String) animals.get(1);
        System.out.println(animal);

        //////////////after generic//////////
        List<String>animals2 = new ArrayList<>();
        animals2.add("cat");
        animals2.add("dog");
        animals2.add("frog");

        String animal2 = animals2.get(1);

        //////////////java7//////////
        List<String> animals3 = new ArrayList<>();
    }


}
class Animal{

}