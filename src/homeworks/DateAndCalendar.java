/*
package homeworks;


import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.*;
import java.util.Calendar;
import java.util.TimeZone;

public class DateAndCalendar {
    public static void main(String[] args) {
        //Методы LocalDate
        LocalDate today = LocalDate.now();//now() возвращают соответствующие объекты содержащие текущую дату и время.
        System.out.println("Current date:" + today);

        LocalDate specificDate = LocalDate.of(2020,Month.NOVEMBER,10);// of() Для создания объекта класса
        System.out.println(specificDate);

        //isBefore Определяет, находится ли установленный день перед текущей датой
        System.out.println("Today is before the 11.11.2019?: " + today.isBefore(LocalDate.of(2019,11,11)));

        //isAfter Определяет, находится ли установленный день перед текущей датой
        System.out.println("Today is after the 01.11.2019?: "+today.isAfter(LocalDate.of(2018,12,2)));

        LocalDate lastDayOfYear = today.with(TemporalAdjusters.lastDayOfYear());
        Period period = today.until(lastDayOfYear);//until определяет, сколько осталось лет до конца года
        System.out.println("Time between to dates is "+period);

        System.out.println(today.format(DateTimeFormatter.BASIC_ISO_DATE));//format() устанавливает формат даты

        System.out.println(today.minus(12, ChronoUnit.YEARS));//minus() вычитание  лет в дате

        System.out.println(today.plus(12, ChronoUnit.YEARS));//plus() прибавление  лет в дате

        LocalDate zonedlt = LocalDate.parse("2018-12-06");//parse()  для получения экземпляра LocalTime из строки
        System.out.println(zonedlt);

        //Методы LocalDateTime:
        LocalDateTime localDateTime = LocalDateTime.now();
        ZonedDateTime zoneDateTime = localDateTime.atZone(ZoneId.of("Europe/Moscow"));// atZone() add zone information.
        System.out.println(zoneDateTime);

        LocalTime time = LocalTime.parse("16:12:49");
        ZoneOffset offset = ZoneOffset.ofTotalSeconds(7200);
        OffsetTime offsettime = time.atOffset(offset);//atOffset комбинирует это время со смещением для создания OffsetTime.
        System.out.println(offsettime);

        TemporalAccessor temporalAccessor = ZonedDateTime.now();//Интерфейс TemporalAccessor — это справочник для запроса
        // отдельной частичной информации по текущей точке или метке и его реализуют все временные классы нового API.

        LocalDateTime dtCheck = LocalDateTime.from(temporalAccessor);//from извлекает дату из temporalAccessor
        System.out.println(dtCheck);

        System.out.println(localDateTime.truncatedTo(ChronoUnit.DAYS));//truncatedTo возвращает копию LocalDateTime с укороченным временем.

        ValueRange r = localDateTime.range(ChronoField.MILLI_OF_DAY);//range возвращает объект ValueRange, xnj имеет информацию о мин и макс допустимых значениях для предоставленного поля.
        System.out.printf("MILLI_OF_DAY: %s%n", r);

        //Методы Calendar
        //getInstance()возвращает объект класса GregorianCalendar, инициированный текущей датой и временем согласно рег. настройкам
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));// getTimeZone()	возвращает часовой пояс
        System.out.println(cal.getTime());

        cal.add(Calendar.DATE, 20);// сдвинуть дату на определённый период с помощью метода add ()
        System.out.println(cal.getTime());

        cal.set(Calendar.YEAR, 2017);//Все поля календаря можно устанавливать по отдельности с помощью метода set().

        System.out.println(cal.getTime());

    }

}
*/
