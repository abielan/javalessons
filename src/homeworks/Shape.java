
package homeworks;

public abstract class Shape {

    public abstract void drawShape();
}

class Square extends Shape {

    @Override
    public void drawShape() {
        System.out.println("Drowing Square");
    }
}

class Circle extends Shape {

    @Override
    public void drawShape() {
        System.out.println("Drowing Sircle");
    }
}

class Test {
    public static void main(String[] args) {

        Circle circle = new Circle();
        Square square = new Square();

        circle.drawShape();
        square.drawShape();
    }
}


