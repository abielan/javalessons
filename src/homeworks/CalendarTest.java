package homeworks;

import java.time.Month;
import  java.time.LocalDate;

public class CalendarTest {
    public static void main(String[] args) {
        CalendarApplication calendarApplication = new CalendarApplication();
        Event event = new Event(LocalDate.of(2019,Month.NOVEMBER,21), "Salut");
        Event event1 = new Event(LocalDate.of(2018,Month.NOVEMBER,21), "Fire");
//
        calendarApplication.addEvent(event);
        calendarApplication.addEvent(event1);

        //1)
        calendarApplication.showTimeAndDate("America/Montreal");

        //2)
        calendarApplication.printEvent();
        calendarApplication.deletetEvent(event1);
        calendarApplication.printEvent();

        //3)
        calendarApplication.defineTimeZoneOfCity("Europe/Berlin");

        //4)
        calendarApplication.getDatePlusMonth(2);

        calendarApplication.getDatePlusWeek(2);

        calendarApplication.getDatePlusYear(2);

        //5)
        calendarApplication.getDaysToNewYear();

        //6)
        /*SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z");
        System.out.println("date: " + dateFormat.format(new Date()));*/


    }
}
