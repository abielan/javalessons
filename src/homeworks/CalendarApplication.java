package homeworks;

import java.time.*;
import java.time.temporal.TemporalAdjusters;
import java.util.Objects;


public class CalendarApplication {

    private Event[] events;

    public CalendarApplication() {
        events = new Event[10];
    }

    public void addEvent(Event event) {
        for (int i = 0; i < events.length; i++) {
            if (Objects.isNull(events[i])) {
                events[i] = event;
                break;
            }
        }
    }

    //1)
    public void showTimeAndDate(String zone) {//show
        ZoneId zoneId = ZoneId.of(zone);
        LocalDate date = LocalDate.now(zoneId);

        System.out.println(date);//should use classes from java.time package

        for (Event event : events) {
            if (Objects.nonNull(event) && event.getDate().equals(date)) {
                System.out.println(event);
            }
        }


    }
    //2)
    public void printEvent() {
        for (Event event : events) {
            if (event !=null){
                System.out.println(event);
            }
        }
        System.out.println("\n");
    }

    public void deletetEvent (Event eventForDelete) {
        for (int i = 0; i <events.length ; i++) {
            if (eventForDelete == events[i]){
                events[i] = null;
            }
        }
    }

    //3)
    public void defineTimeZoneOfCity(String city) {
        ZoneId zone = ZoneId.of(city);
        LocalDateTime dt = LocalDateTime.now();
        ZonedDateTime zdt = dt.atZone(zone);
        DayOfWeek dotw = dt.getDayOfWeek();

        System.out.print(zdt + " ");
        System.out.printf("%s", dotw, "\n\t");
    }

    //4)
    public void getDatePlusMonth(int amount) {


        ZonedDateTime justNow = ZonedDateTime.now();
        ZonedDateTime later = justNow.plusMonths(amount);
        System.out.println("\n" + "Date through the " + amount + " months is " + later);
    }

    public void getDatePlusWeek(int amount) {
        ZonedDateTime justNow = ZonedDateTime.now();
        ZonedDateTime later = justNow.plusWeeks(amount);
        System.out.println("\n" + "Date through the " + amount + " weeks is " + later);
    }

    public void getDatePlusYear(int amount) {
        ZonedDateTime justNow = ZonedDateTime.now();
        ZonedDateTime later = justNow.plusYears(amount);
        System.out.println("\n" + "Date through the " + amount + " weeks is " + later);
    }

    //5)
    public void getDaysToNewYear() {
        LocalDate today = LocalDate.now();
        LocalDate lastDayOfYear = today.with(TemporalAdjusters.lastDayOfYear());
        Period period = today.until(lastDayOfYear);//until определяет, сколько осталось лет до конца года
        System.out.println("\nTime between to dates is " + period);
    }
}

