package homeworks;

public class Recursion {

    public int counterEvenNumber;
    public int counterOddNumber;
    public int sumOddNumber;
    public int result;
    public int CountofOne;

    public int evenNumbers(int number){
        if (number == 0){
            System.out.println("Count is " +counterEvenNumber);
            return 0;
        }

        result = number%10;
        if(result%2 == 0){
            counterEvenNumber++;
            System.out.println(result);
        }

        return evenNumbers(number/10);
    }

    public int oddNumbers(int number){
        if (number == 0){
            System.out.println("Count even numbers is " +(4-counterOddNumber));
            System.out.println("Sum of odd numbers is " +sumOddNumber);
            return 0;
        }

        result = number%10;
        if(result%2 != 0){
            sumOddNumber+=result;
            counterOddNumber++;
        }

        return oddNumbers(number/10);
    }

    public int maxOddAndCountofOne(int number){
        if (number == 0){
            System.out.println("Max Odd number  is " +result);
            System.out.println("Sum of odd numbers is " +CountofOne);
            return 0;
        }
        int temp = result;
        result = number%10;
        if(result == 1){
            CountofOne++;
        }

        if(result<temp){
            result = temp;
        }

        return maxOddAndCountofOne(number/10);
    }
}

class  RecursionTest{
    public static void main(String[] args) {

        Recursion recursion = new Recursion();
        //recursion.oddNumbers(1985);
        //recursion.evenNumbers(1985);
        recursion.maxOddAndCountofOne(1985);

    }
}

