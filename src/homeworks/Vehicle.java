package homeworks;

import java.util.ArrayList;
import java.util.List;

public abstract class Vehicle {

    private int wheels;
    private String fuel;
    private List<Fuel> fuels;

    public Vehicle(int wheels, String fuel) {
        this.wheels = wheels;
        this.fuel = fuel;
        fuels = new ArrayList<>();
    }

    public void addFuel(Fuel fuel) {
        fuels.add(fuel);
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public abstract void accelerate();

    public abstract void brake();
}

abstract class Car extends Vehicle {
    public Car(int wheels, String fuel) {
        super(wheels, fuel);
    }

    @Override
    public void accelerate() {
    }

    @Override
    public void brake() {
    }
}

class Truck extends Vehicle {

    public Truck(int wheels, String fuel) {
        super(wheels, fuel);
        System.out.println(wheels + ", " + fuel);
    }

    @Override
    public void accelerate() {
        System.out.println("Truck accelerate \n");
    }

    @Override
    public void brake() {
        System.out.println("Truck brake \n");
    }
}

class Bus extends Vehicle {

    public Bus(int wheels, String fuel) {
        super(wheels, fuel);
        System.out.println(wheels + ", " + fuel);
    }

    @Override
    public void accelerate() {
        System.out.println("Bus accelerate \n");
    }

    @Override
    public void brake() {
        System.out.println("Bus brake");
    }
}

class Suv extends Car {

    public Suv(int wheels, String fuel) {
        super(wheels, fuel);
        System.out.println(wheels + ", " + fuel);
    }

    @Override
    public void accelerate() {
        System.out.println("Suv accelerate \n");
    }

    @Override
    public void brake() {
        System.out.println("Suv brake");
    }
}

class RacingCar extends Car {

    public RacingCar(int wheels, String fuel) {
        super(wheels, fuel);
        System.out.println(wheels + ", " + fuel);
    }

    @Override
    public void accelerate() {
        System.out.println("RacingCar accelerate \n");
    }

    @Override
    public void brake() {
        System.out.println("RacingCar brake \n");
    }
}

class CompactCar extends Car {

    public CompactCar(int wheels, String fuel) {
        super(wheels, fuel);
        System.out.println(wheels + ", " + fuel);
    }

    @Override
    public void accelerate() {
        System.out.println("CompactCar accelerate \n");
    }

    @Override
    public void brake() {
        System.out.println("CompactCar brake \n");
    }
}

interface Fuel {
    void charge();
}

class Gaz implements Fuel {
    @Override
    public void charge() {
        System.out.println("Can charge by gaz");
    }
}

class Petrol implements Fuel {
    @Override
    public void charge() {
        System.out.println("Can charge by petrol");
    }
}

class Electricity implements Fuel {
    @Override
    public void charge() {
        System.out.println("Can charge by electricity");
    }
}

class TestVehicle {

    public static void main(String[] args) {

        Suv suv = new Suv(4, "gasoline");
        suv.accelerate();

        Truck truck = new Truck(8, "gasoline");
        truck.brake();

        Car racingCar = new RacingCar(4, "diesel");
        racingCar.accelerate();
    }
}