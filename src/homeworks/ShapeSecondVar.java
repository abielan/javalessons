package homeworks;

enum ShareType {
    SQUARE, CIRCLE
}

class ShapeSecondVar {

    public void drawSquare(ShareType shareType) {
        switch (shareType) {
            case SQUARE:
                System.out.println("Drowing Square");
                break;
            case CIRCLE:
                System.out.println("Drowing Sircle");
                break;
        }
    }


    public static void main(String[] args) {

        ShapeSecondVar shapeSecondVar = new ShapeSecondVar();
        shapeSecondVar.drawSquare(ShareType.CIRCLE);
        shapeSecondVar.drawSquare(ShareType.SQUARE);
    }
}
