package homeworks;

import java.time.LocalDate;

public class Event {
    private LocalDate date;
    private String name;

    public Event(LocalDate localDate, String name) {
        this.date = localDate;
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Event{" +
                "date=" + date +
                ", name='" + name + '\'' +
                '}';
    }
}
