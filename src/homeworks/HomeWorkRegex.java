package homeworks;

import java.util.regex.Pattern;

public class HomeWorkRegex {
    public static void main(String[] args) {
        //     Any number of letters, and then two numbers and backward.
        /* String str = "Hrrrrrr55454oK";

        String pattern = "[a-zA-Z]{1,600}[0-9]{2}[0-9]{1,600}[a-zA-Z]{2}";*/

        //     2-4 numbers, and then 2-4 letters and backward.
       String str = "5544HrrroK44";

        String pattern = "[0-9]{2,4}[a-zA-Z]{2,4}[a-zA-Z]{2,4}[0-9]{2,4}";

       //      User inputs name and surname. Check to make sure the thirst letters were uppercase, and other - lowercase
       /* String str = "Ivanov Petr";

        String pattern = "[A-Z]{1}[a-z]{2,40}\\s[A-Z]{1}[a-z]{2,40}";*/

       // The string contains words and numbers. It is necessary to select numbers and calculate their sum


        System.out.println(Pattern.matches(pattern, str));



    }

}
