package homeworks.online_shop;

import java.util.*;

public class OnlineShop {

    List<Product> products = new ArrayList<>();//move to constructor
    List<Product> productsUserBasket = new ArrayList<>();
    private Map<User, List<Product>> basket;

    public OnlineShop() {
        basket = new HashMap<>();
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void printProduct() {
        for (Product product : products) {//iterator
            if (Objects.nonNull(product)) {//Objects
                System.out.println(product);
            }
        }
        System.out.println("\n");
    }

    public void printUserBasket() {
        for (Product product : productsUserBasket) {
            if (product != null) {
                System.out.println(product);
            }
        }
        System.out.println("\n");
    }

    public void removeProduct(Category category) {
        Iterator<Product> productIterator = products.iterator();

        while (productIterator.hasNext()) {
            Product nextCat = productIterator.next();

            if (nextCat.getCategory().equals(category)) {
                productIterator.remove();
            }
        }
    }

    public void addProductToUserBasket(String user, String product) {
        User foundUser =
                basket.keySet().stream().filter(u -> u.getFirstName().equals(user)).findFirst().orElse(null);

        Product product1 = products
                .stream()
                .filter(p -> Objects.equals(p.getProductName(), product))
                .findFirst()
                .orElse(null);

        if (Objects.nonNull(foundUser) && Objects.nonNull(product1)) {
            List<Product> products = basket.get(foundUser);

            products.add(product1);

            product1.changeProductCount();
        }
    }

    public void chooseProductByManufacturer(String manufacturer) {//Product product
        Iterator<Product> productIterator = products.iterator();

        while (productIterator.hasNext()) {
            Product nextCat = productIterator.next();

            if (nextCat.getManufacturer().equals(manufacturer)) {
                System.out.println(products);
            }
        }
    }

}
