package homeworks.online_shop;

import java.time.LocalDate;
import java.time.Month;

public class OnlineShopTest {
    public static void main(String[] args) {
        OnlineShop onlineShop = new OnlineShop();

        Product telephoneNokia = new Product(Category.MOBILE, "Nokia", "Nokia3110", LocalDate.of(2019, Month.NOVEMBER, 21), 450, 5);
        Product coverNokia = new Product(Category.MOBILE, "Nokia", "Cover", LocalDate.of(2019, Month.NOVEMBER, 21), 20, 5);
        Product charge = new Product(Category.MOBILE, "Xiaomi", "Charge", LocalDate.of(2015, Month.NOVEMBER, 20), 12, 13);

        Product tvSony = new Product(Category.TV, "Sony", "Sony5", LocalDate.of(2015, Month.NOVEMBER, 20), 950, 5);
        Product antenna = new Product(Category.TV, "Sony", "Antenna", LocalDate.of(2015, Month.NOVEMBER, 21), 40, 5);

        onlineShop.addProduct(telephoneNokia);
        onlineShop.addProduct(coverNokia);
        onlineShop.addProduct(charge);

        onlineShop.addProduct(tvSony);
        onlineShop.addProduct(antenna);


        onlineShop.printProduct();

        onlineShop.removeProduct(Category.MOBILE);

        onlineShop.printProduct();

        //onlineShop.addProductToUserBasket(tvSony);

        onlineShop.printUserBasket();

        onlineShop.printProduct();

        onlineShop.chooseProductByManufacturer("Sony");

        Calculation calculation = (a, b) -> a + b;

        System.out.println(calculation.sum(tvSony.getProductCount(), antenna.getProductCount()));



    }
}
