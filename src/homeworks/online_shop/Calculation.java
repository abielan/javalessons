package homeworks.online_shop;

@FunctionalInterface
public interface Calculation {
    int sum(int a, int b);

}
