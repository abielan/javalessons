package homeworks.online_shop;

import java.time.LocalDate;

public class Product {

    private String manufacturer;//enum
    private String productName;
    private LocalDate manufactureDate;
    private double price;
    private int productCount;
    private Category category;

    public Product(Category category, String manufacturer, String productName, LocalDate manufactureDate,
                   double price, int productCount) {
        this.manufacturer = manufacturer;
        this.productName = productName;
        this.manufactureDate = manufactureDate;
        this.price = price;
        this.productCount  = productCount;
        this.category = category;
    }

    public String getProductName() {
        return productName;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getProductCount() {
        return productCount;
    }

    public void changeProductCount() {
        this.productCount =  this.productCount - 1;
    }

    public Category getCategory() {
        return category;
    }


    @Override
    public String toString() {
        return "Product{" +category+
                ", manufacturer = " + manufacturer +
                ", ProductName = " + productName +", ManufactureDate = " + manufactureDate +", Price = " + price  +
                ",  Count = " + productCount + '\'' +
                '}';
    }
}
