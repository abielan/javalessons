package homeworks;

import java.util.Random;
import java.util.Scanner;

public class ImitationArrayList {
    private int[] array;
    public static final Random RANDOM = new Random();


    public ImitationArrayList(int size) {
        array = new int[size];
    }

    //TODO Only for test purpose
    public int[] getArray() {
        return array;
    }

    //TODO Only for test purpose
    public void setArray(int[] array) {
        this.array = array;
    }

    /*public int inputValue() {//remove static

        System.out.println("Input array dimension Value: ");
        int value = input.nextInt();
        System.out.println("You have inputed  Value  " + value);

        return valueofArray;
    }*/

    public void addElement(int element) {//1 2 3 8 8 8  -> 8
        resize();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                array[i] = element;
                break;
            }
        }
    }

    private void resize() {
        int index = array.length - 1;

        if (array[index] != 0) {
            int[] temp = new int[array.length * 2];//19

            for (int i = 0; i < array.length; i++) {
                temp[i] = array[i];
            }
            array = temp;;
        }
    }


    public void changeElementByIndex(int index, int value) {//add validation 100
        if (index > array.length - 1 && value != 0) {

            System.out.println("Index is incorrect or value equal to 0");//throw exception

            return;
        }

        array[index] = value;
    }

    public void deleteElementByIndex(int index) {

        int j;
        for (j = index; j < array.length - 1; j++) {
            array[j] = array[j + 1];
        }

        int[] arrNew = new int[j];

        for (int k = 0; k < arrNew.length; k++) {
            arrNew[k] = array[k];
        }

        array = arrNew;
    }

    public void printArrayForward() {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }
    }

    public void printArrayReverse() {
        for (int j = array.length - 1; j >= 0; j--) {
            System.out.print(array[j] + "\t");
        }
    }

    public void increaseLength(int index, int numberOfElements) {

        int[] arrNew = new int[array.length + numberOfElements];

        int j;
        for (j = index; j < arrNew.length - numberOfElements; j++) {
            arrNew[j + numberOfElements] = array[j];
        }

        for (j = index - 1; j >= 0; j--) {
            arrNew[j] = array[j];
        }
        array = arrNew;
    }

    public void reduceLength(int index, int numberOfElements) {

        int[] arrNew = new int[array.length - numberOfElements];

        int j;
        for (j = index; j < array.length - numberOfElements; j++)
            array[j] = array[j + numberOfElements];

        for (int k = 0; k < arrNew.length; k++) {
            arrNew[k] = array[k];
        }

        array = arrNew;
    }

    public void bubbleSortArray() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    public void addArray() {//add array as param
        int[] newArray = new int[5];

        int[] resultArray = new int[array.length + newArray.length];

        for (int i = 0; i < array.length; i++) {
            resultArray[i] = array[i];
        }

        for (int j = array.length, i = 0; j < resultArray.length; j++, i++) {
            resultArray[j] = newArray[i];
        }

        array = resultArray;

    }

    public void deleteDuplicateFromArray() {
        int valueOfResultArray = 0;
        int[] tempArray = new int[10];
        int[] countArray = new int[10];

        for (int i = 0; i < array.length; i++)
            countArray[array[i]]++;
        //Определим размерность результирующего массива и объявим результирующий массив
        for (int i = 0; i < 10; i++) {
            if (countArray[i] != 0) {
                valueOfResultArray++;
            }
        }

        int[] resultArray = new int[valueOfResultArray];

        System.out.println();

        for (int i = 0, j = 0; i < array.length; i++) {
            if (tempArray[array[i]] == 0)
                resultArray[j] = array[i];
            tempArray[array[i]]++;
            j++;
        }

        array = resultArray;
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i] + " ");

        }
        /*for (int i = 0; i < array.length; i++) {//1 2 3 4 5 6 0
            int element = array[i];

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] == element) {
                    deleteElementByIndex(j);
                    --j;
                }
            }
        }*/
    }

    public int linearSearch(int key) {
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                index = i;
                break;
            }
        }
        return index;
    }

    public void shuffleArray(int value) {


        for (int i = 0; i < value; i++)
            array[i] = RANDOM.nextInt(10);
    }

    public static void main(String[] args) {
        Main.menu();
    }
}

class Main {

    public static void menu() {
        int numberOfMenu;
        ImitationArrayList imitationArrayList = new ImitationArrayList(8);

        System.out.println("Input number 1-10: " +
                "\n1 - adding elements, " +
                "\n2 - change elements by index, " +
                "3 - delete elements by index, " +
                "4 - increase the length of the list by a specified number of elements, " +
                "5  - reducing the length of the sheet to a specified number of elements, " +
                "6 - output elements to the console in forward order, " +
                "7 - output elements to the console in reverse order, " +
                "8 - sorting of the sheet by bubble method, " +
                "9 - adding an array to an array, " +
                "10 - remove duplicates, " +
                "11 - find the position of an element using linear search, " +
                "12 - shuffle  elements of list in random order");

        Scanner input = new Scanner(System.in);

        do {
            numberOfMenu = input.nextInt();
            System.out.println("You have inputed a number " + numberOfMenu);
            switch (numberOfMenu) {
                case 1:
                    imitationArrayList.addElement(5);
                    break;
                case 2:
                    imitationArrayList.changeElementByIndex(3, 8);
                    break;
                case 3:
                    System.out.println("\nInput index: ");
                    imitationArrayList.deleteElementByIndex(5);
                    break;
                case 4:
                    System.out.println("\nInput index of array for insert: ");

                    int index = input.nextInt();

                    System.out.println("\nInput number of elements: ");
                    int numberOfElements = input.nextInt();
                    imitationArrayList.increaseLength(index, numberOfElements);
                    break;
                case 5:
                    System.out.println("Input index of array for delete: ");

                    index = input.nextInt();

                    System.out.println("Input number of elements for delete: ");
                    numberOfElements = input.nextInt();
                    imitationArrayList.reduceLength(index, numberOfElements);
                    break;
                case 6:
                    imitationArrayList.printArrayForward();
                    break;
                case 7:
                    imitationArrayList.printArrayReverse();
                    break;
                case 8:
                    imitationArrayList.bubbleSortArray();
                    break;
                case 9:
                    imitationArrayList.addArray();
                    break;
                case 10:
                    imitationArrayList.deleteDuplicateFromArray();
                    break;
                case 11:
                    imitationArrayList.linearSearch(2);
                    break;
                case 12:
                    imitationArrayList.shuffleArray(8);
                    break;
            }
        }
        while (numberOfMenu != 0);
    }
}

