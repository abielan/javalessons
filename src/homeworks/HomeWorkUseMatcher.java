package homeworks;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HomeWorkUseMatcher {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\d");
        int count = 0;

        String str = "1word2word3word";

        Matcher matcher = pattern.matcher(str);

        while (matcher.find()) {
            count = count + Integer.parseInt(matcher.group());
        }
        System.out.println(count);
    }
}

class HomeWorkUseCondition {
    public static void main(String[] args) {
        String str = "(067)677-56-65";
        String pattern = "[(](063|093|095|097|073|067|099)[)][0-9]{3}-[0-9]{2}-[0-9]{2}";

        System.out.println(Pattern.matches(pattern, str));

/*
        String subRegex = str.substring(0, 5);//


        switch (subRegex) {
            case "(097)":
            case "(067)":
                System.out.println("User has Kyivstar number");
                break;
            case "(095)":
            case "(099)":
                System.out.println("User has Vodafone number");
            case "(073)":
            case "(063)":
                System.out.println("User has Life number");

        }
*/

        String operatorRegex = "(\\d{3})";


        Matcher matcher = Pattern.compile(operatorRegex).matcher(str);

        String operator = null;

        while (matcher.find()) {
            operator = matcher.group();
            break;
        }

        System.out.println(operator);
        //switch
    }
}
