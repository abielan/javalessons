package homeworks;

public abstract class Reference {

    protected String author1;
    protected String author2;
    protected String author3;
    protected String author4;
    protected Boolean isUpdated;
    protected int year;
    protected int volume;

    public  Reference (String author1, String author2, String author3, String author4, Boolean isUpdated, int year, int volume) {
        this.author1 = author1;
        this.author2 = author2;
        this.author3 = author3;
        this.author4 = author4;
        this.isUpdated = isUpdated;
        this.year = year;
        this.volume = volume;
    }

    public abstract void processing();


    public String getAuthor1() {
        return author1;
    }

    public String getAuthor2() {
        return author2;
    }

    public String getAuthor3() {
        return author3;
    }

    public String getAuthor4() {
        return author4;
    }

    public Boolean getUpdated() {
        return isUpdated;
    }

    public int getYear() {
        return year;
    }

    public int getVolume() {
        return volume;
    }

}

class Book extends Reference {

    private int countOfPages;
    private String nameBook;

    public Book (String author1, String author2, String author3, String author4, int countOfPages, String nameBook, Boolean isUpdated, int year, int volume){
        super(author1, author2, author3, author4, isUpdated, year, volume);
        this.countOfPages = countOfPages;
        this.nameBook = nameBook;
    }

    @Override
    public void processing()  {
        System.out.println(author1+", "+author2+", "+author3+", "+author4+", "+countOfPages+", "+nameBook+", "+isUpdated+", "+year+", "+volume);
    }


    public int getCountOfPages() {
        return countOfPages;
    }

    public String getNameBook() {
        return nameBook;
    }

}

 class Article extends Reference {
    private int startPage;
    private int endPage;
    private String nameArticle;

    public Article (String author1, String author2, String author3, String author4, int startPage, int endPage,  String nameArticle, Boolean isUpdated, int year, int volume){
         super(author1, author2, author3, author4, isUpdated, year, volume);
         this.startPage = startPage;
         this.endPage = endPage;
         this.nameArticle = nameArticle;
     }

    @Override
    public void processing() {
         System.out.println(author1+", "+author2+", "+author3+", "+author4+", "+startPage+", "+endPage+", "+nameArticle+", "+isUpdated+", "+year+", "+volume);
    }


     public int getStartPage() {
         return startPage;
     }

     public int getEndPage() {
         return endPage;
     }

     public String getNameArticle() {
         return nameArticle;
     }
 }



class TestReference {
    public static void main(String[] args) {

        Book book = new Book("Romanov", "Petrov", "Fedorov","Ivanov",67, "Reference", true, 1989, 100 );
        book.processing();

        Article article = new Article("Bogdanov", "Sergeev", "Ivanova","Petrova",2, 5, "Something", true, 1999, 22);
        article.processing();
    }
}


class RefactorInheritance {

    public void processing(Book book) {
//        do something with book

    }

    public void processing(Article article) {


//        do something with article

//        Заменить параметры на класс Reference, предложить рефакторинг
    }
}

