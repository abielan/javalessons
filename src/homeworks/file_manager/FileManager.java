package homeworks.file_manager;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;


public class FileManager {

    public static final String TXT_EXTENSION = ".txt";

    public static final String COMMON_DIR = "./ForFileManager";// Hello.txt

    public static final String TARGET_DIR = "./ForFileManager/Test";// Hello.txt
    public static final String PDF_EXTENSION = ".pdf";

    public void createTxtFile(Path path) throws IOException {

        Files.createFile(path);
    }

    public void createDir(Path path) throws IOException {

        Files.createDirectory(path);

    }

    public void convertTextToPDF(Path source, Path target) throws IOException {

        Files.copy(source, target, REPLACE_EXISTING, COPY_ATTRIBUTES, NOFOLLOW_LINKS);
        Document document = new Document(PageSize.LETTER);
        try {
            String filePath = target.toString();

            byte[] fileArray = Files.readAllBytes(target);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filePath));
            String str = new String(fileArray);

            document.open();
            document.add(new Paragraph(str));
            document.close();
            writer.close();

            //Path path = Paths.get(document);

        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }
    }

     public void copyFile(Path source, Path target) {//refactor, send two Path variables
         source = Paths.get(COMMON_DIR, "Hello.txt");

        Path newDir = Paths.get(TARGET_DIR, source.getFileName().toString());
        try {
            Files.copy(source, target, REPLACE_EXISTING, COPY_ATTRIBUTES, NOFOLLOW_LINKS);
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public void copyFilesTest(Path source, Path target) {//refactor, send two Path variables

        try {
            Files.copy(source, target, REPLACE_EXISTING, COPY_ATTRIBUTES, NOFOLLOW_LINKS);
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public void removeFile(Path path) {


        try {
            Files.delete(path);
        } catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n", path);
        } catch (IOException x) {
            // File permission problems are caught here.
            System.err.println(x);
        }
    }

    public void removeDir(Path path) {

        try {
            Files.delete(path);
        } catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n", path);
        } catch (DirectoryNotEmptyException x) {
            System.err.format("%s no empty%n", path);
        } catch (IOException x) {
            System.err.println(x);
        }
    }

    public int readDirectory(Path dir) {

        int i = 0;
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path file : stream) {
                System.out.println(file.getFileName());
                i++;
            }
        } catch (IOException | DirectoryIteratorException x) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can only be thrown by newDirectoryStream.
            System.err.println(x);
        }
        return i;

    }

    public void renameFiles(Path source, String newName) throws IOException {

        Files.move(source, source.resolveSibling(newName));
    }

    public void renameDir(Path source, String newName) throws IOException {

        Files.move(source, source.resolveSibling(newName));
    }
}
