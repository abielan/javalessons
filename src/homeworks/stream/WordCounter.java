package homeworks.stream;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class WordCounter {
    public static void main(String[] args) {
        String string = "Проблеми визначення предмету теорії держави і права у визначенні теорії";

        Map<String, Integer> map = new HashMap<>();

        String[] strings = string.split(" ");

        IntStream.range(0, strings.length).forEach(i -> {
            String word = strings[i];

            int count = 1;

            if (map.containsKey(word)) {
                Integer countWord = map.get(word);

                count = ++countWord;
            }

            map.put(word, count);

        });

        System.out.println(map);

    }
}
