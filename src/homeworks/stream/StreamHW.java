package homeworks.stream;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class StreamHW {
    public static void main(String[] args) {
        String string = "Проблеми визначення предмету теорії держави і права у визначенні теорії";
/*

        List<String> str = Arrays.asList(string);

        long count = str.stream().flatMap(m -> {
            String[] split = m.split(" ");
            return Arrays.asList(split).stream();
        }).count();

        System.out.println(count);
*/

        Address address1 = new Address("Ukraine", "Kyiv", "Shevchenko", 2);
        Address address3 = new Address("Ukraine", "Kyiv", "Shevchenko", 4);
        Address address2 = new Address("Canada", "Toronto", "Street of Toronto ", 55);


        List<Man> man = List.of(
                new Man(address1, 25, "Ivana", "Romanova", 2),
                new Man(address3, 25, "Ivan", "Romanov", 2),
                new Man(address1, 25, "Petro", "Fetrov", 2),
                new Man(address1, 25, "Petr", "Petrov", 2),
                new Man(address1, 25, "Peter", "Pullov", 2),
                new Man(address2, 48, "Mark", "Markov", 5));

        man = new LinkedList<>(man);
/*
        //"SELECT * FROM Man";
        man.stream().forEach(System.out::println);

        //"SELECT * FROM Address";
        man.stream().map(Man::getAddress).forEach(System.out::println);//*/

        //"SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName";+++++++++++++++
        Man man1 = new Man("Rogov", "Ivan", 38, 2);
        Man man2 = new Man("Ivanov", "Petr", 28, 3);
        Man man3 = new Man("Smolov", "Fedor", 28, 3);
        List<Man> men = new LinkedList<>();
        men.add(man1);
        men.add(man2);
        men.add(man3);

        Predicate<Man> predicate = m -> m.getAge() >= 20;

       /* List<Man> filteredList =
                men.stream()
                        .filter(predicate)
                        .sorted(Comparator.comparing(Man::getSurname))
                        .collect(Collectors.toList());//  comparator++++++++++++++++++++++++++

         System.out.println("\n" + filteredList);*/
      /*  //"UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US' (or another country)
        man.stream().filter(m -> m.getAddress().getCountry().equals("Ukraine")).forEach(m -> {
            m.setName("John");
            m.setCountChildren(3);
            m.setSurname("Kennedi");

        });
        man.stream().forEach(System.out::println);

        //  "SELECT firstName, lastName, nameOfStreet FROM Man WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";
        System.out.println("\n");
        Predicate<Man> predicateAge = m -> m.getAge() > 20;
        Predicate<Man> predicateHouseNumber = m -> m.getAddress().getHouseNumber() == 55;
        Predicate<Man> predicateCountry = m -> m.getAddress().getCountry().equals("Canada");

        man.stream().filter(predicateAge.and(predicateHouseNumber).and(predicateCountry))
                .forEach(m -> System.out.println(m.getSurname() + " " + m.getName() + " " + m.getAddress().getStreet() + "\n"));

        //"SELECT count(*) FROM Man GROUP BY countOfChildren"
        Map<Integer, List<Man>> mapGroupingBycountOfChildren = man.stream().collect(Collectors.groupingBy(Man::getCountChildren));
        System.out.println(mapGroupingBycountOfChildren + "\n");

        //"SELECT count(*) FROM Man GROUP BY countOfChildren, age"
        List<Man> mans = List.of(
                new Man(1, 25),
                new Man(1, 32),
                new Man(2, 40),
                new Man(2, 42),
                new Man(2, 40),
                new Man(4, 40));

                mans
                        .stream()
                        .collect(Collectors.groupingBy(m -> new AbstractMap.SimpleEntry<>(m.getCountChildren(), m.getAge()), Collectors.counting()));
            *//*          .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 4)
                .forEach(System.out::println);*/

        //"SELECT count(*) FROM Address GROUP BY city, nameOfStreet";
            /*man
                .stream()
                .map(Man::getAddress)
                .collect(Collectors.groupingBy(m -> new AbstractMap.SimpleEntry<>(m.getCity(), m.getStreet()), Collectors.counting()));
*/
        //"SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 4";

          /*  man
                .stream()
                .map(Man::getAddress)
                    .collect((Collectors.groupingBy(m -> new AbstractMap.SimpleEntry<>(m.getCity(), m.getStreet()), Collectors.counting())))
                    .entrySet()
                    .stream()
                    .filter(entry -> entry.getValue() > 3)
                    .forEach(System.out::println);
*/


        //"SELECT count(*) FROM Man GROUP BY city, nameOfStreet";
           /* man
                .stream()
                .collect((Collectors.groupingBy(m -> new AbstractMap.SimpleEntry<>(m.getAddress().getCity(), m.getAddress().getStreet()), Collectors.counting())))
                .entrySet()
                .stream().forEach(System.out::println);*/

        //      "SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 4";,
        man
                .stream()
                .collect((Collectors.groupingBy(m -> new AbstractMap.SimpleEntry<>(m.getAddress().getCity(), m.getAddress().getStreet()), Collectors.counting())))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 5)
                .forEach(System.out::println);


    }

}

class Man {
    private Address address;
    private int age;
    private String name;
    private String surname;
    private int countChildren;


    public Man(Address address, int age, String name, String surname, int countChildren) {
        this.address = address;
        this.age = age;
        this.name = name;
        this.surname = surname;
        this.countChildren = countChildren;
    }

    public Man(String surname, String name, int age, int countChildren) {
        this.surname = surname;
        this.age = age;
        this.name = name;
        this.countChildren = countChildren;
    }

    public Man(int countChildren, int age) {
        this.age = age;
        this.countChildren = countChildren;
    }


    public Man(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setCountChildren(int countChildren) {
        this.countChildren = countChildren;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getCountChildren() {
        return countChildren;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }


    @Override
    public String toString() {
        return "Man{" +
                "address" + address +
                "age=" + age +
                ", name='" + name +
                ", surname='" + surname +
                ", Count of children='" + countChildren + '\'' +
                '}';
    }

    public static void main(String[] args) {
        /*Man one = new Man(12, "").one();

        one.two();*/
    }
}

class Address {
    private String country;
    private String city;
    private String street;
    private int houseNumber;


    public Address(String country, String city, String street, int houseNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;


    }

    public Address(String city, String street) {
        this.city = city;
        this.street = street;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }


    @Override
    public String toString() {
        return "Address{" +
                "Country=" + country +
                ", City='" + city + ", Street='" + street + ", HouseNumber ='" + houseNumber + '\'' +
                '}';
    }


}