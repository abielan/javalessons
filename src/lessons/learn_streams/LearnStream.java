package lessons.learn_streams;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LearnStream {
    public static void main(String[] args) {
        List<Man> men = List.of(
                new Man(17, "Ben"),
                new Man(18, "John"),
                new Man(15, "Jack"),
                new Man(30, "Steeve")
        );

        men = new ArrayList<>(men);

//        men.stream().forEach(m -> System.out.println(m));

        Predicate<Man> predicate = m -> m.getAge() < 20;

        List<Man> filteredList = men.stream().filter(predicate).collect(Collectors.toList());

//        List<Person> people = men.stream().map(m -> new Person(m.getAge(), m.getName())).collect(Collectors.toList());
//        int sum = men.stream().mapToInt(m -> m.getAge()).sum();
        men.stream().mapToInt(Man::getAge).anyMatch(age -> age > 10);

        Map<Integer, List<Man>> map = men.stream().collect(Collectors.groupingBy(m -> m.getAge() % 2));

        System.out.println();

//        Collections.sort(men);

//        System.out.println(men);

        Comparator<Man> comparator = (m1, m2) -> Integer.compare(m1.getAge(), m2.getAge());

        Collections.sort(men, comparator.reversed());

        System.out.println(men);

    }
}

class Person {
    private int age;
    private String name;

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }
}

class Man implements Comparable<Man> {
    private int age;
    private String name;

    public Man(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public Man(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public Man one() {
        return this;
    }

    public Man two() {
        return this;
    }

    @Override
    public int compareTo(Man o) {
        return Integer.compare(o.getAge(), this.age);
    }

    @Override
    public String toString() {
        return "Man{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Man one = new Man(12, "").one();

        one.two();
    }
}

class ReferenceOnConstructor {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("John", "Ben", "Trevor");

//        List<Man> collect = names.stream().map(Man::new).collect(Collectors.toList());

        ManFactory factory = Man::new;

        Man man = factory.createMan(13, "Lora");
    }
}

@FunctionalInterface
interface ManFactory {
    Man createMan(int age, String name);
}
