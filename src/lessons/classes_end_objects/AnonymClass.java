package lessons.classes_end_objects;

interface AbleToEat{
    public void eat();
}


public class AnonymClass {
    public static void main(String[] args) {
        AbleToEat ableToEat = new AbleToEat() {
            @Override
            public void eat() {
                System.out.println("Something");
            }
        };

    }

}
