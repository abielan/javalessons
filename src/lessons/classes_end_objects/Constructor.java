package lessons.classes_end_objects;

public class Constructor {
    public static void main(String[] args) {
        Woman human1 = new Woman("p",5);
    }
}

class Woman{
    private String name;
    private int age;

    public Woman() {
        System.out.println("first constructor");
    }

    public Woman(String name) {
        System.out.println("second constructor");
        this.name = name;
    }

    public Woman(String name, int age) {
        System.out.println("third constructor");
        this.name = name;
        this.age = age;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAge(int age){
        this.age = age;
    }
}