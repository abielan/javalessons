package lessons.classes_end_objects;

public class GuessGame {
    Player p1;
    Player p2;
    Player p3;

    public void startGame(){
        p1 = new Player();
        p2 = new Player();
        p3 = new Player();

        int guessp1;
        int guessp2;
        int guessp3;

        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;

        int targetNumber = (int)(Math.random()*10);
        System.out.println("I made a number fron 0 to 9 ...");
        while (true){
            System.out.println("The Number  should guess - " + targetNumber);
            p1.guess();
            p2.guess();
            p3.guess();

            guessp1 = p1.number;
            System.out.println("The first player think that this number  is" +guessp1);
            guessp2 = p2.number;
            System.out.println("The second player think that this number  is" +guessp2);
            guessp3 = p3.number;
            System.out.println("The third player think that this number  is" +guessp3);

            if (guessp1 == targetNumber) {
                p1isRight = true;
            }
            if (guessp2 == targetNumber){
                p2isRight = true;
            }
            if (guessp3 == targetNumber){
                p3isRight = true;
            }

            if(p1isRight || p2isRight || p3isRight){
                System.out.println("We have a winner!");
                System.out.println("The first player guessed?"  +p1isRight);
                System.out.println("The second player guessed?" +p2isRight);
                System.out.println("The third player guessed?" +p3isRight);
                System.out.println("the end of game");
                break;
            }else{
                System.out.println("The players should attempt another");
            }
        }
    }
}

class Player{
    int number = 0;
    public void guess(){
        number = (int)(Math.random()*10);
        System.out.println("Maybe this number is " + number);
    }
}

class GameLauncher{
    public static void main(String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}