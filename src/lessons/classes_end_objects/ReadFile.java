package lessons.classes_end_objects;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ReadFile {

    public static void main(String[] args) throws IOException {

        Path source = Paths.get("testt");

        Scanner scanner = new Scanner(source);
        while(scanner.hasNextLine()){
            System.out.println(scanner.nextLine());
        }

        scanner.close();
    }



}
