package lessons.classes_end_objects;


public class KeywordThis {
    public static void main(String[] args) {
        Man human1 = new Man();
        human1.setAge(18);
        human1.setName("Tom");
        human1.getInfo();
    }
}

class Man {
    String name;
    int age;

    public void setName(String theName) {
        name = theName;
    }

    public void setAge(int myAge) {
        age = myAge;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void getInfo() {
        System.out.println(name + ", " + age);
    }
}
