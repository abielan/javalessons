package lessons.inheritance.overloading_overriding;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.sql.SQLDataException;

public class Human {

    Number print(String name) throws IOException {
        System.out.println("Human");
        return 5;
    }

    void print(String name, int i) throws SQLDataException {
        System.out.println("Human");
    }
}

class Student extends Human {

    @Override
    public Integer print(String name) throws FileAlreadyExistsException, FileNotFoundException {
        System.out.println("Student");
        return 3;
    }
}

class TestHuman {
    public static void main(String[] args) throws IOException, SQLDataException {
//        int i = "";

        Human human = new Student();//new Human

        human.print("", 5);

    }
}
