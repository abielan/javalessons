package lessons.inheritance;

import java.io.FileNotFoundException;
import java.io.IOException;

class Car {
    private String name;//has-a
    private int age;


    public Car(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Car(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {

        if (age < 0) {
            age = 0;
        }
        this.age = age;
    }


  /*  public static void main(String[] args) {
        Car bmw = new Car("BMW");

        age = 500;

//        bmw.setName("Vaz");
        bmw.name = "Vaz";
        bmw.age = -100;

//        bmw.setAge(-100);


    }*/
}

class Mercedes extends Car {//is-a
    private String model;

    public Mercedes(String name, int age, String model) {
        super(name, age);
        this.model = model;
    }
}

class TestCar {
    public static void main(String[] args) {
        Car bmw = new Car("BMW");
         bmw = new Car("Wolks");

//        bmw.setName("Vaz");
 /*       bmw.name = "Vaz";
        bmw.age = -100;*/

        bmw.setAge(-100);


    }
}

/*
* private - visibilty only inside class
* package-private - visibilty only package
* protected - visibilty only package + subclass
* public - visibilty anywhere
* */
