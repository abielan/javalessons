package lessons;

public class LearnCloning {
    public static void main(String[] args) throws CloneNotSupportedException {

        Address address = new Address("Kiev");

        Original original = new Original("john", 12);

        original.setAddress(address);

        Original clone = original.clone();

        clone.getAddress().setName("Dnepr");

        System.out.println(original.getAddress().getName());
    }
}

class Original implements Cloneable {
    private String name;
    private int age;
    private Address address;

    public Original(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    protected Original clone() throws CloneNotSupportedException {
        Original original = (Original) super.clone();
        Address address = original.getAddress().clone();

        original.setAddress(address);


        return original;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

class Address implements Cloneable {
    private String name;

    public Address(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Address clone() throws CloneNotSupportedException {
        return (Address) super.clone();
    }
}
