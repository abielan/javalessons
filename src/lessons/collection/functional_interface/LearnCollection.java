package lessons.collection.functional_interface;

import java.util.*;

public class LearnCollection {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>(20);

        integers.add(5);
        integers.add(6);
        integers.add(7);

//        System.out.println(integers);

        List<Integer> objects = new LinkedList<>();

        objects.add(5);
        objects.add(6);
        objects.add(7);

//        System.out.println(objects);

        /*for (Integer object : objects) {
            System.out.println(object);
        }*/

        /*for (int i = 0; i < objects.size(); i++) {
            System.out.println(objects.get(i));
        }*/

        Iterator<Integer> iterator = objects.iterator();

        ListIterator<Integer> listIterator = objects.listIterator();


        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
