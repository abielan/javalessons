package lessons.collection.functional_interface;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.function.ToIntBiFunction;

public class LearnFI {
    public static void main(String[] args) {
        Calculatable calculatable = (a, b) -> a + b;
        calculatable = (a, b) -> a * b;

//        System.out.println(calculatable.sum(5, 8));

        Convertable<String, Integer> convertable = value -> Integer.parseInt(value);

//        System.out.println(convertable.convert("45") + 5);

        Function<String, Integer> function = param -> Integer.parseInt(param);

//        System.out.println(function.apply("20") + 10);

        Consumer<String> consumer = str -> System.out.println(str.concat("Hello"));

//        consumer.accept("World");

        Supplier<String> supplier = () -> "Hello";

//        System.out.println(supplier.get());

    }
}

@FunctionalInterface
interface Calculatable {
    int sum(int a, int b);
//    int minus(int a, int b);

    default void print() {

    }

    /*private void run() {

    }*/
}


@FunctionalInterface
interface Convertable<F, T> {
    T convert(F value);
}
