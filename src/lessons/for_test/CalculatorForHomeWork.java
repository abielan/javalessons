package lessons.for_test;

import java.util.Random;

public class CalculatorForHomeWork {

    public int sumRandom() { return getRandomValueForOperandOne() + getRandomValueForOperandTwo() ;    }

    public int minusRandom() { return getRandomValueForOperandOne() - getRandomValueForOperandTwo();    }

    public int multiplicationRandom() {  return getRandomValueForOperandOne() * getRandomValueForOperandTwo();  }

    public int divisionRandom() { return getRandomValueForOperandOne() / getRandomValueForOperandTwo();  }

    public int  remainderRandom() { return getRandomValueForOperandOne() % getRandomValueForOperandTwo(); }

    public int getRandomValueForOperandOne() {
        return new Random().nextInt(20);
    }

    public int getRandomValueForOperandTwo() {
        return new Random().nextInt(20);
    }



    public static int sumRandomStatic() { return getValueOperandOne() + getValueOperandTwo() ;}

    public static int getValueOperandOne() {
        return new Random().nextInt(20);
    }

    public static int getValueOperandTwo() {
        return new Random().nextInt(20);
    }


    public int sumPrivate() {return getRandomValuePrivateOperandOne() + getRandomValuePrivateOperandTwo();}

    private int getRandomValuePrivateOperandOne() {
        return new Random().nextInt(20);
    }

    private int getRandomValuePrivateOperandTwo() {
        return new Random().nextInt(20);
    }


}
