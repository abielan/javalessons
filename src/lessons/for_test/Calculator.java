package lessons.for_test;

import java.io.IOException;
import java.util.Random;

public class Calculator {

    public int sum(int a, int b) {
        return a + b;
    }

    public int sumPrivate(int a) {
        return a + getRandomValuePrivate();
    }

    public int sumRandom(int a) {
        return a + getRandomValue();
    }

    public int minus(int a, int b) {

        return a - b;
    }

    public int getRandomValue() {
        return new Random().nextInt(20);
    }

    private int getRandomValuePrivate() {
        return new Random().nextInt(20);
    }

    public static int getValue() {
        return new Random().nextInt(20);
    }

    public void print() {
        System.out.println("Hello world");
    }

    public void thrownException() throws IOException {
        throw new IOException("File doesn't exist");
    }


}