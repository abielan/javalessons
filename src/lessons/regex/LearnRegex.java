package lessons.regex;

import java.util.regex.Pattern;

public class LearnRegex {
    public static void main(String[] args) {
//        Matching any character
        /*String str = "Hello";

        String pattern = "H.llo";*/

//        Matching only specific characters

        /*String str = "Hallo";

        String pattern = "H[Eep]llo";*/

//        Matching range characters[a-kA-K0-9]

        /*String str = "H8llo";

        String pattern = "H[a-kA-K0-9]llo";*/

        //        Matching any digits

        /*String str = "H8llo";

        String pattern = "H\\dllo";*/

        //        Matching non digits

        /*String str = "Hjllo";

        String pattern = "H\\Dllo";*/

        //        Matching word character

        /*String str = "H_llo";

        String pattern = "H\\wllo";//[a-zA-Z0-9_]*/

        //        Matching non word character

        /*String str = "H@llo";

        String pattern = "H\\Wllo";*/

//        Quantifiers

//        "*" multiple times
/*        String str = "Hell";

        String pattern = "Hello*";*/
//        "+" 1 or more times
        /*String str = "Helloo";

        String pattern = "Hello+";*/

        //        "{n}" n times
        //        "{n, m}" from n to m times
        String str = "Heellooo";

        String pattern = "He{2,4}llo{1,3}";

        System.out.println(Pattern.matches(pattern, str));


    }
}
