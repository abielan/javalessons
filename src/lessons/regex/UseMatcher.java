package lessons.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UseMatcher {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\d");

        String str = "1word2word3word";

        Matcher matcher = pattern.matcher(str);

        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }
}

class UseCondition {
    public static void main(String[] args) {
        String regex = ".*(run|gun).*";

        System.out.println(Pattern.matches(regex, "World gun try"));
    }
}
