Создать имитацию коллекции(с помощью массива) ArrayList для работы с типом int.

Создать класс с полем типа массив.

Класс должен выполнять следующие операции:

1) добавление элементов.
2) изменение/удаление элементов по индексу.
3) увеличение длины листа на заданное количество элементов.
4) уменьшение длины листа до заданного количество элементов.
5) вывод элементов в консоль в прямом и обратном порядке.
6) сортировка листа методом пузырька(http://study-java.ru/uroki-java/urok-11-sortirovka-massiva/).
7) добавление массива в массив.
8) удалять дубликаты.
9) Поиск позиции элемента методом линейного поиска.
10) Перемешивание элементов листа в случайном порядке.

При удалении элемента не обнулять его, а удалять физически.
Начальную размерность листа юзер вводит с консоли. 30
Создать меню для работы с листом из консоли.
Условие добавления: перезаписывать если элемент равен 0;
В задаче не использовать методы класса Arrays, System и коллекции.