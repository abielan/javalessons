Create branch + merge

   1. Создать имитацию запросов в базу данных на основе коллекции c применением lambdas.

      Сущности Man(имя, фамилия, возраст, количество детей, Адрес), Адрес(страна, город, улица)

      Использовать коллекцию LinkedList. Запросы для класса Адрес делать из класса Man.

      "SELECT * FROM Man";

      "SELECT * FROM Address";

      "SELECT firstName, lastName, countOfChildren FROM Man WHERE age >= 20 ORDER BY firstName"???;

      "UPDATE Man SET firstName = 'John', lastName = 'Kennedi', countOfChildren = 3 WHERE country == 'US' (or another country)

      "SELECT firstName, lastName, nameOfStreet FROM Man WHERE country == 'Canada' AND numberOfHome == 3 OR age >= 25";

      "SELECT count(*) FROM Man GROUP BY countOfChildren"

      "SELECT count(*) FROM Man GROUP BY countOfChildren, age"

      "SELECT count(*) FROM Address GROUP BY city, nameOfStreet";

      "SELECT count(*) FROM Address GROUP BY city, nameOfStreet HAVING countOfCitizens > 4";

      "SELECT count(*) FROM Man GROUP BY city, nameOfStreet";

      "SELECT count(*) FROM Man GROUP BY city , nameOfStreet HAVING countOfCitizens > 4";

      2. Шифр пропорцианальной замены. Известно, что при использовании шифра пропорцианальной замены каждой русской букве
         соответствует одно или несколько трехзначных чисел по таблице замен.

      3. Создать приложение переводчик.

         Приложение должно позволять:
         - добавлять новые слова.
         - переводить предложение с одного из языков(Русский - Украинский, Украинский - Русский, Английский - Русский, Русский - Английский).
         - после ввода определять язык, на котором ввел юзер
         - добавлять новые языки

         После остановки программы сохранять словари в файлах.
