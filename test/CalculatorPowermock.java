import lessons.for_test.Calculator;
import lessons.for_test.CalculatorForHomeWork;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Calculator.class)
public class CalculatorPowermock {


    @Test
    public void shouldMockStatic() {
        PowerMockito.mockStatic(CalculatorForHomeWork.class);

        Mockito.when(CalculatorForHomeWork.sumRandomStatic()).thenReturn(15);

        Assert.assertEquals(15, Calculator.getValue());
    }

    @Test
    public void shouldMockPrivateMethod() throws Exception {
        CalculatorForHomeWork calculator = new CalculatorForHomeWork();
        CalculatorForHomeWork spyedCalculator = PowerMockito.spy(calculator);

        PowerMockito
                .when(spyedCalculator, MemberMatcher.method(CalculatorForHomeWork.class, "sumPrivate"))
                .withNoArguments()
                .thenReturn(22);

        Assert.assertEquals(22, spyedCalculator.sumPrivate());
    }
}
