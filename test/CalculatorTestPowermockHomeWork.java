
import lessons.for_test.CalculatorForHomeWork;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.api.support.membermodification.MemberMatcher;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorForHomeWork.class)

public class CalculatorTestPowermockHomeWork {
    @Test
    public void shouldMockStatic() {
        PowerMockito.mockStatic(CalculatorForHomeWork.class);

        Mockito.when(CalculatorForHomeWork.getValueOperandOne()).thenReturn(15);
        Mockito.when(CalculatorForHomeWork.getValueOperandTwo()).thenReturn(16);

        Assert.assertEquals(31, CalculatorForHomeWork.getValueOperandOne() + CalculatorForHomeWork.getValueOperandTwo());
    }

    @Test
    public void shouldMockPrivateMethod() throws Exception {
        CalculatorForHomeWork calculator = new CalculatorForHomeWork();
        CalculatorForHomeWork spyedCalculator = PowerMockito.spy(calculator);

        PowerMockito
                .when(spyedCalculator, MemberMatcher.method(CalculatorForHomeWork.class, "getRandomValuePrivateOperandOne"))
                .withNoArguments()
                .thenReturn(12);

        PowerMockito
                .when(spyedCalculator, MemberMatcher.method(CalculatorForHomeWork.class, "getRandomValuePrivateOperandTwo"))
                .withNoArguments()
                .thenReturn(11);

        Assert.assertEquals(23, spyedCalculator.sumPrivate());
    }

}
