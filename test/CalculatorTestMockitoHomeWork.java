import lessons.for_test.CalculatorForHomeWork;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)

public class CalculatorTestMockitoHomeWork {

    @Spy
    private CalculatorForHomeWork calculatorSpy;

    @Mock
    private CalculatorForHomeWork calculatorMock;



    @Test
    public void shouldReturnSumValues() {

        Mockito.when(calculatorSpy.getRandomValueForOperandOne()).thenReturn(8);
        Mockito.when(calculatorSpy.getRandomValueForOperandTwo()).thenReturn(9);

        Assert.assertEquals(17, calculatorSpy.sumRandom());
    }

    @Test
    public void shouldReturnSumValuesMock() {

        Mockito.when(calculatorMock.getRandomValueForOperandOne()).thenReturn(8);
        Mockito.when(calculatorMock.getRandomValueForOperandTwo()).thenReturn(9);

        Mockito.when(calculatorMock.sumRandom()).thenCallRealMethod();

        Assert.assertEquals(17, calculatorMock.sumRandom());
    }

    @Test
    public void shouldReturnMinusValues() {

        Mockito.when(calculatorSpy.getRandomValueForOperandOne()).thenReturn(8);
        Mockito.when(calculatorSpy.getRandomValueForOperandTwo()).thenReturn(9);

        Assert.assertEquals(-1, calculatorSpy.minusRandom());
    }

    @Test
    public void shouldReturnMultiplicationValues() {

        Mockito.when(calculatorSpy.getRandomValueForOperandOne()).thenReturn(8);
        Mockito.when(calculatorSpy.getRandomValueForOperandTwo()).thenReturn(9);

        Assert.assertEquals(72, calculatorSpy.multiplicationRandom());
    }

    @Test
    public void shouldReturnDivisionValues() {

        Mockito.when(calculatorSpy.getRandomValueForOperandOne()).thenReturn(18);
        Mockito.when(calculatorSpy.getRandomValueForOperandTwo()).thenReturn(9);

        Assert.assertEquals( 2, calculatorSpy.divisionRandom());
    }

    @Test
    public void shouldReturnRemainderValues() {

        Mockito.when(calculatorSpy.getRandomValueForOperandOne()).thenReturn(19);
        Mockito.when(calculatorSpy.getRandomValueForOperandTwo()).thenReturn(9);

        Assert.assertEquals( 1, calculatorSpy.remainderRandom());

    }

}
