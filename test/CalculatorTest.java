import lessons.for_test.Calculator;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CalculatorTest {

/*    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("Before all tests");
    }

    @AfterClass
    public static void afterAllTests() {
        System.out.println("After all tests");
    }

    @Before
    public void beforeEachTest() {
        System.out.println("Before each test");
    }

    @After
    public void afterEachTest() {
        System.out.println("After each test");
    }*/

    Calculator calculator = new Calculator();


    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void shouldReturnSumValues() {

        int sum = calculator.sum(5, 2);

        Assert.assertEquals("Should return 7", 7, sum);
    }

    @Test
    public void shouldReturnMinusValues() {

        int minus = calculator.minus(5, 2);

        Assert.assertEquals("Should return 3", 3, minus);
    }

    @Test
    public void shouldTestConsole() {

        calculator.print();

        String log = outRule.getLog();//Helloo world

        Assert.assertTrue(log.contains("Hello world"));

    }


    @Test(expected = IOException.class)
    public void shouldThrowException() throws IOException {
        calculator.thrownException();
    }

    @Test
    public void shouldThrowExceptionUsingRule() throws IOException {

        expectedException.expect(IOException.class);
        expectedException.expectMessage("File doesn't exist");

        calculator.thrownException();

    }

    @Test
    public void testTemporaryFolderRule() throws IOException {
        File file = temporaryFolder.newFile("World.FileManagerTest");

        File folder = temporaryFolder.newFolder("com", "app", "java");

        Path path = Paths.get(folder.getAbsolutePath(), "Tree.FileManagerTest");

        Files.createFile(path);

        Files.write(path, "Good morning".getBytes());

        System.out.println();
    }

}
