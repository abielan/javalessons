import lessons.for_test.Calculator;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class CalculatorParametrizedTest {

   /* @Parameterized.Parameter(value = 1)
    public int valueOne;

    @Parameterized.Parameter
    public int valueTwo;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{1, 1}, {2, 2}};
        return Arrays.asList(data);
    }*/

    @Test
    @Parameters({"-9|-10", "7|5"})
    public void shouldReturnSumValues(int valueOne, int valueTwo) {
        Calculator calculator = new Calculator();

        int sum = calculator.sum(valueOne, valueTwo);

        Assert.assertEquals( valueOne + valueTwo, sum);
    }
}
