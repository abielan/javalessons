import homeworks.ImitationArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ImitationArrayListTest {

    private ImitationArrayList list = new ImitationArrayList(5);

    @Before
    public void clearArray() {
        list.setArray(new int[4]);//5 0
    }

    @Test
    public void shouldAddElementToArray() {
        list.addElement(5);

        int array[] = list.getArray();

        Assert.assertEquals(5, array[0]);
    }

    @Test
    public void shouldChangeElementByIndex() {
        list.addElement(5);
        list.addElement(7);

        list.changeElementByIndex(1, 8);

        int[] array = list.getArray();

        Assert.assertEquals(8, array[1]);
    }

    @Test
    public void shouldDeleteElementByIndex() {
        list.addElement(5);
        list.addElement(7);

        list.deleteElementByIndex(0);

        int[] array = list.getArray();

        Assert.assertEquals(7, array[0]);//check array length
    }

    @Test
    public void shouldReduceLength() {
        list.addElement(4);

        list.reduceLength(0, 2);

        int[] array = list.getArray();

        Assert.assertEquals(0, array[0]);

        Assert.assertEquals(3, array.length);
    }

    @Test
    public void shouldResizeArray() {
        list.addElement(5);
        list.addElement(7);
        list.addElement(9);

        int[] array = list.getArray();

        Assert.assertEquals(4, array.length);
        Assert.assertEquals(9, array[2]);
    }

    @Test
    public void shouldBubbleSortArray() {
        list.addElement(5);
        list.addElement(1);
        list.addElement(9);

        list.bubbleSortArray();
        int[] array = list.getArray();
        Assert.assertEquals(1, array[0]);
        Assert.assertEquals(5, array[1]);
        Assert.assertEquals(9, array[2]);
    }

    @Test
    public void shouldAddArray() {//check sequence of elements
        list.addArray();
        int[] array = list.getArray();

        Assert.assertEquals(8, array.length);

    }

    @Test
    public void shouldDeleteDuplicateFromArray() {
        list.addElement(1);
        list.addElement(3);
        list.addElement(1);
        list.addElement(3);

        list.deleteDuplicateFromArray();

        int[] array = list.getArray();

        Assert.assertEquals(1, array[0]);
        Assert.assertEquals(3, array[1]);

        Assert.assertEquals(2, array.length);

    }

    @Test
    public void shouldLinearSearch() {
        list.addElement(5);
        list.addElement(7);
        list.addElement(9);

        int i = list.linearSearch(5);

        Assert.assertEquals(0, i);

    }

    @Test
    public void shouldIncreaseLength() {
        list.addElement(5);

        list.increaseLength(0, 2);

        int[] array = list.getArray();

        Assert.assertEquals(0, array[0]);
        Assert.assertEquals(0, array[1]);
        Assert.assertEquals(5, array[2]);
        Assert.assertEquals(4, array.length);

    }

}

/*
* array = 2 els.
*
* add - 7
* increase - 2 -> 7 0 0 0
* change -> 7 0 2 0
* delete -> 7 0 2
* deteteDuplicates
*
*
* */
