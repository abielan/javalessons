import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;


import homeworks.file_manager.FileManager;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class FileManagerTest {
    public static final String TXT_EXTENSION = ".txt";

    public static final String COMMON_DIR = "./ForFileManager";// Hello.txt

    @Rule
    public TemporaryFolder tf = new TemporaryFolder();

    private FileManager fileManager = new FileManager();

    @Test
    public void shouldCreateTxtFile() throws IOException {

        File comFolder = tf.newFolder("com");
        String absolutePathComFolder = comFolder.getAbsolutePath();
        String fileName = "Text.txt";
        Path path = Paths.get(absolutePathComFolder, fileName);

        fileManager.createTxtFile(path);

        Assert.assertTrue(Files.exists(path));
    }

    @Test
    public void shouldCreateDir() throws IOException {

        File comFolder = tf.newFolder("com");
        String absolutePathComFolder = comFolder.getAbsolutePath();
        String dirName = "Text";
        Path path = Paths.get(absolutePathComFolder, dirName);

        fileManager.createDir(path);

        File dir = new File(path.toString());
        Assert.assertEquals(true, dir.exists());//
    }

    @Test
    public void shouldCopyTxtFile() throws IOException {
        File comFolder = tf.newFolder("com");

        File appFolder = tf.newFolder("app");

        String absolutePathComFolder = comFolder.getAbsolutePath();

        String fileName = "Poem.txt";

        Path pathSource = Paths.get(absolutePathComFolder, fileName);

        Files.createFile(pathSource);

        String text = "Hello source";

        Files.write(pathSource, text.getBytes());

        String absolutePathTarget = appFolder.getAbsolutePath();

        Path pathTarget = Paths.get(absolutePathTarget, fileName);

        fileManager.copyFilesTest(pathSource, pathTarget);

        Assert.assertTrue(Files.exists(pathTarget));

        String textTarget = new String(Files.readAllBytes(pathTarget));

        String textTemp = new String(Files.readAllBytes(pathSource));

        Assert.assertEquals(textTemp, textTarget);
    }

    @Test
    public void shouldConvertTextToPDF() throws IOException {
        File comFolder = tf.newFolder("com");

        File appFolder = tf.newFolder("app");

        String absolutePathComFolder = comFolder.getAbsolutePath();

        String fileName = "Text.txt";

        String filePDFName = "Text.pdf";

        Path pathSource = Paths.get(absolutePathComFolder, fileName);

        Files.createFile(pathSource);

        String text = "Hello world!!!!!!!!!!!!!!!!!!!";

        Files.write(pathSource, text.getBytes());

        String absolutePathTarget = appFolder.getAbsolutePath();

        Path pathTarget = Paths.get(absolutePathTarget, filePDFName);

        fileManager.convertTextToPDF(pathSource, pathTarget);

        Assert.assertTrue(Files.exists(pathTarget));

        String textSource = new String(Files.readAllBytes(pathSource));

        byte[] fileArray = Files.readAllBytes(pathTarget);

        PdfReader pdfReader = new PdfReader(fileArray);

        String textTarget = PdfTextExtractor.getTextFromPage(pdfReader, 1);

        Assert.assertEquals(textSource, textTarget);
    }

    @Test
    public void shouldRemoveFile() throws IOException {//witout tf

        File comFolder = tf.newFolder("com");
        String absolutePathComFolder = comFolder.getAbsolutePath();
        String fileName = "Text.txt";
        Path path = Paths.get(absolutePathComFolder, fileName);
        fileManager.createTxtFile(path);
        fileManager.removeFile(path);

        File txtFile = new File(path.toString());
        Assert.assertEquals(false, txtFile.exists());//
    }

    @Test
    public void shouldRemoveDir() throws IOException {

        File comFolder = tf.newFolder("com");
        String absolutePathComFolder = comFolder.getAbsolutePath();
        String dirName = "Text";
        Path path = Paths.get(absolutePathComFolder, dirName);
        fileManager.createDir(path);
        fileManager.removeDir(path);

        File txtFile = new File(path.toString());
        Assert.assertEquals(false, txtFile.exists());//
    }

    @Test
    public void shouldReadDirectory() throws IOException {

        File comFolder = tf.newFolder("com");
        String absolutePathComFolder = comFolder.getAbsolutePath();
        String dirName = "Text";
        Path pathDir = Paths.get(absolutePathComFolder, dirName);

        fileManager.createDir(pathDir);

        String fileName = "Text.txt";
        Path pathFile = Paths.get(absolutePathComFolder, fileName);
        fileManager.createTxtFile(pathFile);

       /* File txtFile = new File(pathFile.toString());
        Assert.assertEquals(true, txtFile.exists());*/
        Path pathCom = Paths.get(absolutePathComFolder);
        int countOfFilesAndDir = fileManager.readDirectory(pathCom);

        Assert.assertEquals(2, countOfFilesAndDir);
    }

    @Test
    public void shouldRenameFiles() throws IOException {
        String oldName = "Text";
        String newName = "Word";
        Path source = Paths.get(COMMON_DIR, oldName.concat(TXT_EXTENSION));
        fileManager.renameFiles(source, "Word");

        Path newSource = Paths.get(COMMON_DIR, newName.concat(TXT_EXTENSION));
        File txtFile = new File(newSource.toString());
        Assert.assertEquals(true, txtFile.exists());//
    }

    @Test
    public void shouldRenameDir() throws IOException {
        String oldName = "Text";
        String newName = "Word";
        Path source = Paths.get(COMMON_DIR, oldName);
        fileManager.renameFiles(source, "Word");

        Path newSource = Paths.get(COMMON_DIR, newName);
        File dir = new File(newSource.toString());
        Assert.assertEquals(true, dir.exists());//
    }

}
