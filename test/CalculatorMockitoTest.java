import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorMockitoTest {

    @Mock
    private Calculator calculatorMock;

    @Spy
    private Calculator calculatorSpy;


    @Test
    public void shouldReturnSumValues() {

//        Calculator calculatorMock = Mockito.mock(Calculator.class);

        Mockito.when(calculatorMock.getRandomValue()).thenReturn(8);

        Mockito.when(calculatorMock.sumRandom(anyInt())).thenCallRealMethod();

        Assert.assertEquals(18, calculatorMock.sumRandom(10));

    }

    @Test
    public void shouldReturnSumValuesUsingSpy() {

        Mockito.when(calculatorSpy.getRandomValue()).thenReturn(8);

        Assert.assertEquals(18, calculatorSpy.sumRandom(10));

    }
}

/*
* A{
* B
* }
* */
